<?php

class Country
{
  public function getCountryNameByCityName($city) {
    $cities = array('Marseille', 'Paris', "Bordeaux", "Lyon");

    if (in_array($city, $cities)) {
      $country = 'France';
    } else {
      $country = 'Unknow';
    }
    return $country;
  }
}
