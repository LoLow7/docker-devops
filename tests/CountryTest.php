<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/Country.php");

class CountryTest extends \PHPUnit\Framework\TestCase
{
   public function testgetCountryNameByCityName()
   {
       $country = new Country();
       $result = $country->getCountryNameByCityName('Bordeaux');
       $expected = 'France';
       
       $this->assertTrue($result == $expected);
   }
}
